package vnext.vn.phomgame.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vnext.vn.phomgame.entity.DetailMatch;
import vnext.vn.phomgame.entity.Match;

import java.util.List;

public interface DetailMatchRepository extends JpaRepository<DetailMatch, Integer> {
    @Query("select detailmatch from DetailMatch detailmatch where detailmatch.idM = ?1")
    List<DetailMatch> getDetailMatchByMatchId(Integer idM);
}
