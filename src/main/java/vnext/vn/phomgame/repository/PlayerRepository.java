package vnext.vn.phomgame.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vnext.vn.phomgame.entity.Player;

import java.util.List;

public interface PlayerRepository extends JpaRepository<Player, Integer> {

}
