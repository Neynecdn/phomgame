package vnext.vn.phomgame.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vnext.vn.phomgame.entity.Match;

import java.util.List;

public interface MatchRepository extends JpaRepository<Match, Integer> {
    @Query("select match from Match match where match.idB = ?1")
    List<Match> getMatchByTableId(Integer idB);
}
