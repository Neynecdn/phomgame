package vnext.vn.phomgame.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vnext.vn.phomgame.entity.Table;

public interface TableRepository extends JpaRepository<Table, Integer> {
}
