package vnext.vn.phomgame.service.impl;

import org.springframework.stereotype.Service;
import vnext.vn.phomgame.entity.DetailMatch;
import vnext.vn.phomgame.repository.DetailMatchRepository;
import vnext.vn.phomgame.service.DetailMatchService;

import java.util.List;

@Service
public class DetailMatchServiceImpl implements DetailMatchService {
    private DetailMatchRepository detailMatchRepository;
    public DetailMatchServiceImpl(DetailMatchRepository detailMatchRepository) {
        super();
        this.detailMatchRepository = detailMatchRepository;
    }

    @Override
    public List<DetailMatch> getAllDetailMatch() {
        return detailMatchRepository.findAll();
    }

    @Override
    public DetailMatch saveDetailMatch(DetailMatch detailMatch) {
        return detailMatchRepository.save(detailMatch);
    }

    @Override
    public DetailMatch getDetailMatchById(Integer idDM) {
        return detailMatchRepository.findById(idDM).get();
    }

    @Override
    public DetailMatch updateDetailMatch(DetailMatch detailMatch) {
        return detailMatchRepository.save(detailMatch);
    }

    @Override
    public void deleteDetailMatchById(Integer idDM) {
        detailMatchRepository.deleteById(idDM);
    }

    @Override
    public List<DetailMatch> getDetailMatchByMatchId(Integer idM) {
        return detailMatchRepository.getDetailMatchByMatchId(idM);
    }


}
