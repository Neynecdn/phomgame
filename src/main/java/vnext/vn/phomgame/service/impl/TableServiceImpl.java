package vnext.vn.phomgame.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vnext.vn.phomgame.entity.Table;
import vnext.vn.phomgame.repository.TableRepository;
import vnext.vn.phomgame.service.TableService;

import java.util.List;

@Service
public class TableServiceImpl implements TableService {
    @Autowired
    private TableRepository tableRepository;

    public TableServiceImpl(TableRepository tableRepository) {
        super();
        this.tableRepository = tableRepository;
    }
    @Override
    public List<Table> getAllTable() {
        return tableRepository.findAll();
    }

    @Override
    public Table saveTable(Table table) {
        return tableRepository.save(table);
    }

    @Override
    public Table getTableById(Integer idB) {
        return tableRepository.findById(idB).get();
    }

    @Override
    public Table updateTable(Table table) {
        return tableRepository.save(table);
    }

    @Override
    public void deleteTableById(Integer idB) {
        tableRepository.deleteById(idB);
    }
}
