package vnext.vn.phomgame.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vnext.vn.phomgame.entity.Player;
import vnext.vn.phomgame.exception.PlayerNotfoundException;
import vnext.vn.phomgame.repository.PlayerRepository;
import vnext.vn.phomgame.service.PlayerService;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    PlayerRepository playerRepository;
    @Override
    public List<Player> getAllPlayer() {
        return playerRepository.findAll();
    }

    @Override
    public Player savePlayer(Player player) {
        return playerRepository.save(player);
    }

    @Override
    public Player getPlayer(Integer id) throws PlayerNotfoundException {
        Optional<Player> player = playerRepository.findById(id);
        if(player.isPresent()) {
            return player.get();
        }
        throw new PlayerNotfoundException("Player khong ton tai!");
    }

    @Override
    public void deletePlayer(Integer id) {
        playerRepository.deleteById(id);
    }
}
