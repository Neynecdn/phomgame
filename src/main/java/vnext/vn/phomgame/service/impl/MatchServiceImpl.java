package vnext.vn.phomgame.service.impl;

import org.springframework.stereotype.Service;
import vnext.vn.phomgame.entity.Match;
import vnext.vn.phomgame.entity.Table;
import vnext.vn.phomgame.repository.MatchRepository;
import vnext.vn.phomgame.service.MatchService;

import java.util.List;

@Service
public class MatchServiceImpl implements MatchService {
    private MatchRepository matchRepository;
    public MatchServiceImpl(MatchRepository matchRepository) {
        super();
        this.matchRepository = matchRepository;
    }

    @Override
    public List<Match> getAllMatch() {
        return matchRepository.findAll();
    }

    @Override
    public Match saveMatch(Match match) {
        return matchRepository.save(match);
    }

    @Override
    public Match getMatchById(Integer idM) {
        return matchRepository.findById(idM).get();
    }

    @Override
    public Match updateMatch(Match match) {
        return matchRepository.save(match);
    }

    @Override
    public void deleteMatchById(Integer idM) {
        matchRepository.deleteById(idM);
    }

    @Override
    public List<Match> getMatchByTableId(Integer idB) {
        return matchRepository.getMatchByTableId(idB);
    }
}
