package vnext.vn.phomgame.service;

import vnext.vn.phomgame.entity.Player;
import vnext.vn.phomgame.exception.PlayerNotfoundException;

import java.util.List;

public interface PlayerService {
    List<Player> getAllPlayer();
    Player savePlayer(Player player);
    Player getPlayer(Integer id) throws PlayerNotfoundException;
    void deletePlayer(Integer id) throws PlayerNotfoundException;

}
