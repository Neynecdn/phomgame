package vnext.vn.phomgame.service;

import vnext.vn.phomgame.entity.Match;
import vnext.vn.phomgame.entity.Table;

import java.util.List;

public interface MatchService {
    List<Match> getAllMatch();

    Match saveMatch(Match match);

    Match getMatchById(Integer idB);

    Match updateMatch(Match match);

    void deleteMatchById(Integer idB);

    List<Match> getMatchByTableId(Integer idB);
}
