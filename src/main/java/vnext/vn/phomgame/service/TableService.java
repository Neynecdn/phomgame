package vnext.vn.phomgame.service;

import vnext.vn.phomgame.entity.Table;

import java.util.List;

public interface TableService {
    List<Table> getAllTable();

    Table saveTable(Table table);

    Table getTableById(Integer idB);

    Table updateTable(Table table);

    void deleteTableById(Integer id);
}
