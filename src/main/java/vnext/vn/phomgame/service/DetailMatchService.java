package vnext.vn.phomgame.service;

import vnext.vn.phomgame.entity.DetailMatch;
import vnext.vn.phomgame.entity.Match;

import java.util.List;

public interface DetailMatchService {
    List<DetailMatch> getAllDetailMatch();

    DetailMatch saveDetailMatch(DetailMatch detailMatch);

    DetailMatch getDetailMatchById(Integer idDM);

    DetailMatch updateDetailMatch(DetailMatch detailMatch);

    void deleteDetailMatchById(Integer idDM);

    List<DetailMatch> getDetailMatchByMatchId(Integer idM);

}
