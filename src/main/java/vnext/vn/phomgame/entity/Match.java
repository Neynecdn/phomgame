package vnext.vn.phomgame.entity;

import jakarta.persistence.*;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "matchs")
public class Match {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idM;
    @Column(name = "idb", nullable = false)
    private Integer idB;
    @Column(name = "date_play", nullable = false)
    @DateTimeFormat(pattern = "yyyy/MM/dd", iso = DateTimeFormat.ISO.DATE)
    private LocalDate datePlay;
    @Column(name = "num_player", nullable = false)
    private Integer numPlayer;
    @Column(name = "num_match", nullable = false)
    private String numMatch;
    @Column(name = "player_win")
    private String playerWin;
    @Column(name = "money_bonus")
    private String moneyBonus;
}
