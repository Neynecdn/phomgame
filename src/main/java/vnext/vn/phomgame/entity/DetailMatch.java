package vnext.vn.phomgame.entity;

import jakarta.persistence.*;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "detailmatchs")
public class DetailMatch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDM;
    @Column(name = "idm", nullable = false)
    private Integer idM;
    @Column(name = "idp", nullable = false)
    private String idP;
    @Column(name = "bet_level", nullable = false)
    private String betLevel;
    @Column(name = "mid_game", nullable = false)
    private String midGame;
    @Column(name = "end_game", nullable = false)
    private String endGame;
    @Column(name = "total_money", nullable = false)
    private Integer totalMoney;
}
