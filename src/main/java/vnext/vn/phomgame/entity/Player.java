package vnext.vn.phomgame.entity;

import jakarta.persistence.*;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(schema = "player")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idP;

    private String namePlayer;

    private String phoneNumber;

    public Player(){}

    public Player(Integer idP, String namePlayer, String phoneNumber) {
        this.idP = idP;
        this.namePlayer = namePlayer;
        this.phoneNumber = phoneNumber;

    }
}
