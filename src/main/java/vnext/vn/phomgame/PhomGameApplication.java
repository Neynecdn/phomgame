package vnext.vn.phomgame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhomGameApplication {

    public static void main(String[] args) {
        SpringApplication.run(PhomGameApplication.class, args);
    }

}
