package vnext.vn.phomgame.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import vnext.vn.phomgame.entity.Player;
import vnext.vn.phomgame.exception.PlayerNotfoundException;
import vnext.vn.phomgame.service.impl.PlayerServiceImpl;

import java.util.List;

@Controller
@RequestMapping("/player")
public class PlayerController {
    @Autowired
    PlayerServiceImpl playerService;

    @GetMapping("/list")
    public String listPlayer(Model model) {
        List<Player> player = playerService.getAllPlayer();
        model.addAttribute("player", player);
        return "player/list-player";
    }


    @GetMapping("/create")
    public String createPlayer(Model model) {
        model.addAttribute("title", "Thêm người chơi");
        model.addAttribute("buttonSave", "Thêm mới");
        model.addAttribute("player", new Player());
        return "player/create-player";
    }

    @PostMapping("/save")
    public String savePlayer(Player player, RedirectAttributes ra) {
        ra.addFlashAttribute("message", "Lưu người chơi thành công.");
        playerService.savePlayer(player);
        return "redirect:/player/list";
    }

    @GetMapping("/edit/{id}")
    public String editPlayer(@PathVariable Integer id, Model model) {
        try {
            model.addAttribute("title", "Sửa người chơi id :" + id);
            model.addAttribute("buttonSave", "Sửa");
            Player player = playerService.getPlayer(id);
            model.addAttribute("player", player);
            return "player/create-player";
        } catch (PlayerNotfoundException e) {
            return "redirect:/player/list";
        }
    }

    @GetMapping("/{id}")
    public String removePlayer(@PathVariable Integer id, RedirectAttributes ra) {
        playerService.deletePlayer(id);
        ra.addFlashAttribute("message","The user ID " + id + " has been deleted.");
        return "redirect:/player/list";
    }
}
