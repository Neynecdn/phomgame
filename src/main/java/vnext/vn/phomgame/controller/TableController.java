package vnext.vn.phomgame.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import vnext.vn.phomgame.entity.Table;
import vnext.vn.phomgame.service.TableService;

@Controller
public class TableController {
    private final TableService tableService;

    public TableController(TableService tableService) {
        super();
        this.tableService = tableService;
    }

    @GetMapping("/tables")
    public String listTable(Model model) {
        model.addAttribute("tables", tableService.getAllTable());
        return "table/tables";
    }
    @GetMapping("/tables/create")
    public String createTable(Model model) {
        Table table = new Table();
        model.addAttribute("table", table);
        return "table/create_tables";
    }

    @PostMapping("/tables")
    public String saveTable(@ModelAttribute("table") Table table) {
        tableService.saveTable(table);
        return "redirect:/tables";
    }

    @GetMapping("/tables/edit/{idB}")
    public String editTable(@PathVariable Integer idB, Model model) {
        model.addAttribute("table", tableService.getTableById(idB));
        return "table/edit_tables";
    }

    @PostMapping("/tables/{idB}")
    public String updateTable(@PathVariable Integer idB, @ModelAttribute("table") Table table, Model model) {
        Table existingTable = tableService.getTableById(idB);
        existingTable.setIdB(idB);
        existingTable.setNumTable(table.getNumTable());
        existingTable.setBetLevel(table.getBetLevel());
        existingTable.setDatePlay(table.getDatePlay());

        tableService.updateTable(existingTable);
        return "redirect:/tables";
    }

    @GetMapping("/tables/{idB}")
    public String deleteTable(@PathVariable Integer idB) {
        tableService.deleteTableById(idB);
        return "redirect:/tables";
    }
}
