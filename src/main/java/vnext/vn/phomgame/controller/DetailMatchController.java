package vnext.vn.phomgame.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import vnext.vn.phomgame.entity.DetailMatch;
import vnext.vn.phomgame.entity.Match;
import vnext.vn.phomgame.entity.Player;
import vnext.vn.phomgame.service.DetailMatchService;
import vnext.vn.phomgame.service.TableService;
import vnext.vn.phomgame.service.impl.MatchServiceImpl;
import vnext.vn.phomgame.service.impl.PlayerServiceImpl;

import java.util.List;

@Controller
public class DetailMatchController {
    private final DetailMatchService detailMatchService;
    private final PlayerServiceImpl playerService;
    private final MatchServiceImpl matchService;
    private final TableService tableService;

    public DetailMatchController(DetailMatchService detailMatchService, PlayerServiceImpl playerService, MatchServiceImpl matchService, TableService tableService) {
        super();
        this.detailMatchService = detailMatchService;
        this.playerService = playerService;
        this.matchService = matchService;
        this.tableService = tableService;
    }

    @GetMapping("/detailmatchs")
    public String listDetailMatch(Model model) {
        model.addAttribute("detailmatchs", detailMatchService.getAllDetailMatch());
        return "detail_match/detailmatchs";
    }

    @GetMapping({"/detailmatchs/create", "/detailmatchs/create/{idM}"})
    public String createDetailMatch(@PathVariable Integer idM, Model model, RedirectAttributes ra) {
        DetailMatch detailMatch = new DetailMatch();
        int countDetailM = detailMatchService.getDetailMatchByMatchId(idM).size();
        int countNumberP = matchService.getMatchById(idM).getNumPlayer();
        if (countDetailM >= countNumberP) {
            ra.addFlashAttribute("message", "Tối đa chỉ có " + countNumberP + " người chơi!");
            return "redirect:/detailmatchs/matchs/" + idM;
        }
        if (idM != null) {
            detailMatch.setBetLevel(tableService.getTableById(matchService.getMatchById(idM).getIdB()).getBetLevel());
            detailMatch.setIdM(idM);
        }
        model.addAttribute("countPlayer", matchService.getMatchById(idM).getNumPlayer());
        model.addAttribute("detailmatch", detailMatch);
        List<Player> players = playerService.getAllPlayer();
        model.addAttribute("players", players);
        return "detail_match/create_detailmatchs";
    }

    @PostMapping("/detailmatchs")
    public String saveDetailMatch(@ModelAttribute("detailmatch") DetailMatch detailMatch) {
        detailMatchService.saveDetailMatch(detailMatch);
        Integer idM = detailMatch.getIdM();
        return "redirect:/detailmatchs/matchs/" + idM;
    }

    @GetMapping("/detailmatchs/edit/{idDM}")
    public String editDetailMatch(@PathVariable Integer idDM, Model model) {
        model.addAttribute("detailmatch", detailMatchService.getDetailMatchById(idDM));
        List<Player> players = playerService.getAllPlayer();
        model.addAttribute("players", players);
        List<Match> matchs = matchService.getAllMatch();
        model.addAttribute("matchs", matchs);
        return "detail_match/edit_detailmatchs";
    }

    @PostMapping("/detailmatchs/{idDM}")
    public String updateDetailMatch(@PathVariable Integer idDM, @ModelAttribute("detailmatchs") DetailMatch detailMatch, Model model) {
        DetailMatch existingDetailMatch = detailMatchService.getDetailMatchById(idDM);
        existingDetailMatch.setIdDM(idDM);
        existingDetailMatch.setIdP(detailMatch.getIdP());
        existingDetailMatch.setIdM(detailMatch.getIdM());
        existingDetailMatch.setBetLevel(detailMatch.getBetLevel());
        existingDetailMatch.setMidGame(detailMatch.getMidGame());
        existingDetailMatch.setEndGame(detailMatch.getEndGame());
        existingDetailMatch.setTotalMoney(detailMatch.getTotalMoney());

        detailMatchService.updateDetailMatch(existingDetailMatch);
        Integer idM = detailMatch.getIdM();
        return ("redirect:/detailmatchs/matchs/") + idM;
    }

    @GetMapping("/detailmatchs/delete/{idDM}")
    public String deleteDetailMatch(@PathVariable Integer idDM, @ModelAttribute("detailmatch") DetailMatch detailMatch) {
        Integer idM = detailMatchService.getDetailMatchById(idDM).getIdM();
        detailMatchService.deleteDetailMatchById(idDM);
        return "redirect:/detailmatchs/matchs/" + idM;
    }

    @GetMapping("/detailmatchs/matchs/{idM}")
    public String getListDetailMatchByMatch(@PathVariable Integer idM, Model model) {
        model.addAttribute("detailmatchs", detailMatchService.getDetailMatchByMatchId(idM));
        model.addAttribute("idRb", idM);
        return "detail_match/detailmatchs";
    }
}
