package vnext.vn.phomgame.controller;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import vnext.vn.phomgame.entity.Match;
import vnext.vn.phomgame.entity.Table;
import vnext.vn.phomgame.service.MatchService;
import vnext.vn.phomgame.service.impl.TableServiceImpl;

import java.util.List;


@Controller
public class MatchController {
    private final MatchService matchService;
    private final TableServiceImpl tableService;
    public MatchController(MatchService matchService, TableServiceImpl tableService) {
        super();
        this.matchService = matchService;
        this.tableService = tableService;
    }


    @GetMapping("/matchs")
    public String listMatch(Model model) {
        model.addAttribute("matchs", matchService.getAllMatch());
        return "match/matchs";
    }


    @GetMapping("/matchs/create")
    public String createMatch(Model model) {
        Match match = new Match();
        model.addAttribute("match", match);
        List<Table> tables = tableService.getAllTable();
        model.addAttribute("tables", tables);
        return "match/create_matchs";
    }

    @GetMapping("/matchs/create/{idB}")
    public String createMatch(@PathVariable Integer idB ,Model model) {
        Match match = new Match();
        Table table = this.tableService.getTableById(idB);
        Integer numberM = matchService.getMatchByTableId(idB).size()+1;
        if (idB != null){
            match.setIdB(idB);
            match.setNumMatch(String.valueOf(numberM));
            match.setDatePlay(table.getDatePlay());
        }
        model.addAttribute("match", match);
        List<Table> tables = tableService.getAllTable();
        model.addAttribute("tables", tables);
        return "match/create_matchs";
    }

    @PostMapping("/matchs")
    public String saveMatch(@ModelAttribute("match") Match match) {
        matchService.saveMatch(match);
        Integer idB = match.getIdB();
        return "redirect:/matchs/tables/" + idB;
    }

    @GetMapping("/matchs/edit/{idM}")
    public String editMatch(@PathVariable Integer idM, Model model) {
        model.addAttribute("match", matchService.getMatchById(idM));
        List<Table> tables = tableService.getAllTable();
        model.addAttribute("tables", tables);
        return "match/edit_matchs";
    }

    @PostMapping("/matchs/{idM}")
    public String updateMatch(@PathVariable Integer idM, @ModelAttribute("match") Match match, Model model) {
        Match existingMatch = matchService.getMatchById(idM);
        existingMatch.setIdM(idM);
        existingMatch.setIdB(match.getIdB());
        existingMatch.setDatePlay(match.getDatePlay());
        existingMatch.setNumMatch(match.getNumMatch());
        existingMatch.setPlayerWin(match.getPlayerWin());
        existingMatch.setMoneyBonus(match.getMoneyBonus());

        matchService.updateMatch(existingMatch);
        Integer idB = match.getIdB();
        return "redirect:/matchs/tables/" + idB;
    }

    @GetMapping("/matchs/delete/{idM}")
    public String deleteMatch(@PathVariable Integer idM, @ModelAttribute("match") Match match) {
        Integer idB = matchService.getMatchById(idM).getIdB();
        matchService.deleteMatchById(idM);
        return "redirect:/matchs/tables/" + idB;
    }

    @GetMapping("/matchs/tables/{idB}")
    public String getListMatchByTable(@PathVariable Integer idB, Model model) {
        model.addAttribute("matchs", matchService.getMatchByTableId(idB));
        model.addAttribute("idRa", idB);
        return "match/matchs";
    }
}
